module DiscountRules

  def apply_user_costs_rules_per_day(query)
    return [] if query.nil? || query.empty?

    query.map do |item|
      array_item = {
        id: item.id,
        name: item.name,
        transactions: []
      }
      total_sells = 0
      total_buies = 0

      item.bills_to_pay.each do |bills_to_pay|
        total_sells = bills_to_pay.price
        data = {
          date: bills_to_pay.maturity_date.strftime("%d/%m/%Y"),
          sells: {},
        }
        bills_to_pay_obj = {
          price: total_sells,
        }

        if array_item[:transactions].empty?
          data[:sells] = bills_to_pay_obj
          array_item[:transactions].push(data)
        else
          correct_index = nil
          array_item[:transactions].each_with_index do |item, index|
            if item[:date] == data[:date]
              correct_index = index
              break
            end
          end
          if !correct_index.nil? && !array_item[:transactions][correct_index].nil?
            if array_item[:transactions][correct_index][:sells].nil?
              array_item[:transactions][correct_index][:sells] = bills_to_pay_obj
            else
              array_item[:transactions][correct_index][:sells][:price] += bills_to_pay_obj[:price]
            end
          else
            data[:sells] = bills_to_pay_obj
            array_item[:transactions].push(data)
          end
        end
      end

      item.receivement.each do |receivement|
        total_buies = receivement.price
        data = {
          date: receivement.receivement_date.strftime("%d/%m/%Y"),
          buies: {},
        }
        receivement_obj = {
          price: total_buies,
        }

        if array_item[:transactions].empty?
          data[:buies] = receivement_obj
          array_item[:transactions].push(data)
        else
          correct_index = nil
          array_item[:transactions].each_with_index do |item, index|
            if item[:date] == data[:date]
              correct_index = index
              break
            end
          end
          if !correct_index.nil? && !array_item[:transactions][correct_index].nil?
            if array_item[:transactions][correct_index][:buies].nil?
              array_item[:transactions][correct_index][:buies] = receivement_obj
            else
              array_item[:transactions][correct_index][:buies][:price] += receivement_obj[:price]
            end
          else
            data[:buies] = receivement_obj
            array_item[:transactions].push(data)
          end
        end
      end

      array_item
    end
  end

  def apply_client_costs_rules(query, detailed = false)
    if !query.nil? &&
       !query.empty?
      result = []

      query.each do |item|
        if ( !item.sell.nil? && !item.sell.empty? ) ||
          ( !item.receivement.nil? && !item.receivement.empty? )
          array_item = {
            id: item.id,
            name: item.name,
            total_sell: 0,
            total_buy: 0,
            transactions: []
          }

          item.sell.each do |sell_item|
            product = get_item_by_id(sell_item.product_id, item.product)
            discount_value = 0

            if !product.nil?
              price = check_prices(product.buy_price, product.sell_price)
              sell_price = apply_discount(
                sell_item.quantity,
                price,
                sell_item.discount_type,
                sell_item.discount
              )
              array_item[:total_sell] += sell_price
              if detailed
                array_item[:transactions].push({
                  id: sell_item.id,
                  type: 'sell',
                  date: sell_item.created_at,
                  price: sell_price,
                })
              end
            end

          end

          item.receivement.each do |receivement|
            array_item[:total_buy] += receivement.price
            if detailed
              array_item[:transactions].push({
                id: receivement.id,
                type: 'receivement',
                date: receivement.receivement_date,
                price: receivement.price,
              })
            end
          end

          if detailed && !array_item[:transactions].nil?
            array_item[:transactions] = array_item[:transactions].sort {|x,y| y[:date] <=> x[:date] }
          end

          result.push(array_item)
        end
      end

      return result
    end

    return []
  end

  private
    def get_item_by_id(id, sell_groups)
      if !sell_groups.nil? &&
        !sell_groups.empty?
        return sell_groups.detect{|e| e.id == id}
      end

      return nil
    end

    def check_prices(buy_price, sell_price)
      if !buy_price.nil? && (sell_price.nil? || sell_price == 0)
        return buy_price
      end

      return sell_price
    end

    def apply_discount(quantity = 0, price = 0, discount_type = 'price', discount_value = 0)
      item_full_price = price * quantity

      if !discount_value.nil? && !item_full_price.nil? && !discount_type.nil?
        if discount_type == 'price'
          price = item_full_price - discount_value
          return floor(price * 100 / 100.0, 2)
        else
          price = item_full_price - (item_full_price * discount_value) / 100
          return floor(price * 100 / 100.0, 2)
        end
      else
        return floor(item_full_price * 100 / 100.0, 2)
      end
    end

    def floor(value ,exp = 0)
      multiplier = 10 ** exp
      ((value * multiplier).floor).to_f/multiplier.to_f
    end
end
