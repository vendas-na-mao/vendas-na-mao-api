module PagSeguroHelper
  require 'uri'
  require 'net/http'

  def get_pagseguro_session
    url = URI("#{ENV['pagseguro_url']}/v2/sessions")
    url.query = URI.encode_www_form({
      email: ENV['pagseguro_email'],
      token: ENV['pagseguro_token']
    })
    
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'application/xml;charset=ISO-8859-1'
    
    response = http.request(request)

    return Hash.from_xml(response.read_body) rescue { error: 'unauthorized' }
  end

  def subscribe_pagseguro_plan(data)
    url = URI("#{ENV['pagseguro_url']}/pre-approvals")
    url.query = URI.encode_www_form({
      email: ENV['pagseguro_email'],
      token: ENV['pagseguro_token']
    })
    
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'application/json'
    request["accept"] = 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'
    request.body = data.to_json
    
    response = http.request(request)

    return JSON.parse(response.read_body) rescue { error: 'unauthorized' }
  end

  def get_pagseguro_preapproval_notification(notification)
    url = URI("#{ENV['pagseguro_url']}/pre-approvals/notifications/#{notification}")
    url.query = URI.encode_www_form({
      email: ENV['pagseguro_email'],
      token: ENV['pagseguro_token']
    })
    
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Get.new(url)
    request['content-type'] = 'application/json'
    request['accept'] = 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'
    
    response = http.request(request)

    return JSON.parse(response.read_body) rescue { error: 'unauthorized' }
  end

  def get_pagseguro_preapprovals
    date_time = DateTime.new(2017, 11, 1).localtime("-03:00")
    end_date_time = date_time + 30.years

    url = URI("#{ENV['pagseguro_url']}/pre-approvals/request?email=#{ENV['pagseguro_email']}&token=#{ENV['pagseguro_token']}&startCreationDate=#{date_time.strftime("%Y-%m-%dT%H:%M:%S.%3N%:z")}&endCreationDate=#{end_date_time.strftime("%Y-%m-%dT%H:%M:%S.%3N%:z")}&status=ACTIVE")
    
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    
    request = Net::HTTP::Get.new(url)
    request["accept"] = 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'

    response = http.request(request)

    return JSON.parse(response.read_body) rescue { error: 'unauthorized' }
  end

  def get_pagseguro_preapproval(code)
    url = URI("#{ENV['pagseguro_url']}/pre-approvals/#{code}?email=#{ENV['pagseguro_email']}&token=#{ENV['pagseguro_token']}")
    
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    
    request = Net::HTTP::Get.new(url)
    request["accept"] = 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'

    response = http.request(request)

    return JSON.parse(response.read_body) rescue { error: 'unauthorized' }
  end

  def get_pagseguro_preapproval_payments(code, page = 1)
    url = URI("#{ENV['pagseguro_url']}/pre-approvals/#{code}/payment-orders?email=#{ENV['pagseguro_email']}&token=#{ENV['pagseguro_token']}&page=#{page}")
  
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    
    request = Net::HTTP::Get.new(url)
    request["accept"] = 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'

    response = http.request(request)

    data = JSON.parse(response.read_body) rescue { error: 'unauthorized' }
    if data[:error].nil? && !data['paymentOrders'].nil?
      data_array = []
      data['paymentOrders'].each do |key, item|
        item['status'] = get_transaction_status(item['status'])
        data_array.push(item)
      end

      return data_array
    end

    return data
  end

  def change_pagseguro_preapproval_payment_method(code, data = {})
    url = URI("#{ENV['pagseguro_url']}/pre-approvals/#{code}/payment-method?email=#{ENV['pagseguro_email']}&token=#{ENV['pagseguro_token']}")
  
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    
    request = Net::HTTP::Put.new(url)
    request["content-type"] = 'application/json'
    request["accept"] = 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'
    request.body = data.to_json

    response = http.request(request)

    result = JSON.parse(response.read_body) rescue { error: 'unauthorized' }

    if (!result['error'].nil? && result['error'] == true) && result['errors']
      return {
        status: 400,
        data: {
          error: get_pagseguro_errors(result['errors'])
        }
      }
    end

    return {
      data: result,
      status: response.code
    }
  end

  def change_pagseguro_preapproval_status(code, data = {})
    url = URI("#{ENV['pagseguro_url']}/pre-approvals/#{code}/status?email=#{ENV['pagseguro_email']}&token=#{ENV['pagseguro_token']}")
    
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    
    request = Net::HTTP::Put.new(url)
    request["content-type"] = 'application/json'
    request["accept"] = 'application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'
    request.body = data.to_json

    response = http.request(request)

    result = JSON.parse(response.read_body) rescue { error: 'unauthorized' }

    if (!result['error'].nil? && result['error'] == true) && result['errors']
      return {
        status: 400,
        data: {
          error: get_pagseguro_errors(result['errors'])
        }
      }
    end

    return {
      data: result,
      status: response.code
    }
  end

  def get_transaction_status(status)
    case status
    when 1
      'Agendada'
    when 2
      'Processando'
    when 3
      'Não processada'
    when 4
      'Suspensa'
    when 5
      'Paga'
    when 6
      'Não paga'
    else
      'Indefinida'
    end
  end

  def get_pagseguro_errors(errors)
    if errors.is_a?(Hash)
      error_message = "Erro(s) ao enviar dados:\n"
      errors.each do |key, value|
        t_msg = I18n.t("pagseguro.errors.#{key}")
        if !t_msg.index('translation missing')
          error_message += t_msg + "\n"
        else
          error_message += value + "\n"
        end
      end

      return error_message
    else
      return 'Erro ao fazer assinatura, favor tente novamente mais tarde'
    end
  end
end