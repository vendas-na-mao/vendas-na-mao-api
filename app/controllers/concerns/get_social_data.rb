module GetSocialData
  require 'uri'
  require 'net/http'

  def facebook_getdata(access_token)    
    url = URI("https://graph.facebook.com/v2.10/me?fields=name,email,birthday,cover,picture")
    
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Get.new(url)
    request["authorization"] = "OAuth #{access_token}"
    
    response = http.request(request)
    return {
      data: JSON.parse(response.read_body),
      status: response.code,
      social: 'facebook'
    } rescue {
      data: { error: 'Error on get user data.' },
      status: 400,
      social: 'facebook'
    }
  end

  def google_getdata(access_token)
    url = URI("https://www.googleapis.com/plus/v1/people/me")
    
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    
    request = Net::HTTP::Get.new(url)
    request["authorization"] = "OAuth #{access_token}"
    
    response = http.request(request)
    return {
      data: JSON.parse(response.read_body),
      status: response.code,
      social: 'google'
    } rescue {
      data: { error: 'Error on get user data.' },
      status: 400,
      social: 'google'
    }
  end
end