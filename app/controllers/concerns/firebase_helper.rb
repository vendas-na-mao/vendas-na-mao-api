module FirebaseHelper
  require 'uri'
  require 'net/http'

  def send_fcm(data = {})
    url = URI("https://fcm.googleapis.com/fcm/send")
    
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    
    request = Net::HTTP::Post.new(url)
    request["authorization"] = "key=#{ENV['fcm_server_key']}"
    request["content-type"] = 'application/json'
    request.body = data.to_json

    response = http.request(request)
    resp_data = {
      data: response.read_body,
      status: 200
    } rescue {
      data: {
        error: 'Error on send message'
      },
      status: 400
    }
  end
end