class ApplicationController < ActionController::API
  include JwtAuth

  def current_user
    @current_user ||= User.find_by(id: get_user_id) || VoidUser.new
  end

  private

  def get_user_id
    token         = request.headers.fetch('Authorization', '').split
    decoded_token = jwt_decode(token.last)

    decoded_token[0]['user_id'] rescue nil
  end

  class VoidUser
    def method_missing(method, *args, &block)
      self
    end

    def premium?; false end
  end
end
