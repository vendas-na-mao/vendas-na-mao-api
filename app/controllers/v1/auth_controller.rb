module V1
  class AuthController < ApplicationController
    include GetSocialData

    skip_before_action :jwt_auth_validation, only: [:social_auth, :normal_auth, :password_recovery_request, :password_recovery]

    def social_auth
      auth_data = fetch_social_data
      response = {
        error: 'Error on authenticate, check if your token is valid.'
      }

      if (200..299).to_a.index auth_data[:status].to_i
        pass =  (auth_data[:data]['id'].to_i + 1.minute.from_now.to_i).to_s rescue 1.minute.from_now.to_i.to_s

        if auth_data[:social] === 'facebook'
          usrData = {
            name: auth_data[:data]['name'],
            bday: auth_data[:data]['birthday'],
            email: auth_data[:data]['email'],
            password: pass,
            password_confirmation: pass,
            user_type: 1,
            social_auth: true,
            user_social_auth_attributes: {
              social_user_id: auth_data[:data]['id'],
              social_media: 0, # facebook
            }
          }
        else
          usrData = {
            name: auth_data[:data]['displayName'],
            bday: auth_data[:data]['birthday'],
            password: pass,
            password_confirmation: pass,
            user_type: 1,
            social_auth: true,
            user_social_auth_attributes: {
              social_user_id: auth_data[:data]['id'],
              social_media: 1, # google
            }
          }
          if auth_data[:data]['emails'].kind_of?(Array) && !auth_data[:data]['emails'][0]['value'].nil?
            usrData[:email] = auth_data[:data]['emails'][0]['value']
          end
        end

        user = User.find_by(email: usrData[:email])
        if user
          UserSocialAuth.where(social_user_id: usrData[:name])
            .first_or_create!(usrData[:user_social_auth_attributes].merge(user_id: user.id))
        else
          user = User.includes(:user_social_auth)
            .where(name: usrData[:name], email: usrData[:email])
            .first_or_create!(usrData)
        end

        response = {
          user_data: {
            id: user.id,
            name: user.name,
            email: user.email,
            created_at: user.created_at,
          },
          token: get_jwt_by_user(user)
        }
      end

      render json: response, status: auth_data[:status]
    end

    def normal_auth
      auth_data = normal_params
      data = Hash.new

      if auth_data[:email] && auth_data[:password]
        user_type = auth_data[:admin].nil? ? :guest : :admin
        email     = auth_data[:email].to_s.downcase
        user      = User.find_by(email: email, user_type: user_type)

        if user && user.authenticate(auth_data[:password])
          data[:response] = {
            user_data: {
              id: user.id,
              name: user.name,
              email: user.email,
              user_type: user.user_type,
              created_at: user.created_at,
            },
            token: get_jwt_by_user(user, user_type)
          }
          data[:status] = 200
        else
          data[:response] = { error: 'Wrong email or password, please make sure if your caps lock has been pressed or if email is correct and try again.' }
          data[:status] = :unauthorized
        end
      end

      render json: data[:response], status: data[:status]
    end

    def password_recovery_request
      response = {
        data: { error: 'Email not found.' },
        status: 401,
      }
      pass_params = password_request_params
      user_type = !pass_params[:admin].nil? ? :admin : :guest
      user = User.find_by(email: pass_params[:email], user_type: user_type)

      if user
        payload = {
          token_type: 'client_app',
          user_id: user.id,
          exp: 6.hour.from_now.to_i
        }
        token = jwt_encode(payload)
        host = request.protocol + request.host_with_port

        if AuthMailer.password_recovery(host, user.email, user.name, token).deliver_later
          response = {
            data: { success: 'Email has been sent successful' },
            status: 200,
          }
        end
      end

      render json: response[:data], status: response[:status]
    end

    def password_recovery
      tokenDecoded = jwt_decode(params[:token])
      result = {}

      if params[:password] && params[:password_confirmation] && params[:token]
        if tokenDecoded.length > 0 && tokenDecoded[0][:invalid].nil? && !tokenDecoded[0]['user_id'].nil?
          user = User.find_by(id: tokenDecoded[0]['user_id'])
          user.password = params[:password]

          if user.save
            result = { msg: 'Senha alterada com sucesso!' }
          end
        else
          result = { msg: 'Token Expirado.' }
        end
      end

      if tokenDecoded.length > 0 && (!tokenDecoded[0][:invalid].nil? || !tokenDecoded[0][:expired].nil?)
        render_html = recover_invalid_html
      else
        render_html = recover_html(result)
      end
      render html: render_html
    end

    private
      def recover_invalid_html
        "<!DOCTYPE html>
        <html>
        <head>
          <title>Vendas na mão</title>
          <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
          <style>
            body {
              font: normal 18px/1.5 'Fira Sans', 'Helvetica Neue', sans-serif;
              background: #3AAFAB;
              color: #fff;
              padding: 50px 0;
            }

            .container {
              width: 80%;
              max-width: 1200px;
              margin: 0 auto;
            }

            .container * {
              box-sizing: border-box;
            }

            .flex-outer,
            .flex-inner {
              list-style-type: none;
              padding: 0;
            }

            .flex-outer {
              max-width: 800px;
              margin: 0 auto;
            }

            .flex-outer li,
            .flex-inner {
              display: flex;
              flex-wrap: wrap;
              align-items: center;
            }

            .flex-inner {
              padding: 0 8px;
              justify-content: space-between;
            }

            .flex-outer > li:not(:last-child) {
              margin-bottom: 20px;
            }

            .flex-outer li label,
            .flex-outer li p {
              padding: 8px;
              font-weight: 300;
              letter-spacing: .09em;
              text-transform: uppercase;
            }

            .flex-outer > li > label,
            .flex-outer li p {
              flex: 1 0 120px;
              max-width: 220px;
            }

            .flex-outer > li > label + *,
            .flex-inner {
              flex: 1 0 220px;
            }

            .flex-outer li p {
              margin: 0;
            }

            .flex-outer li input:not([type='checkbox']),
            .flex-outer li textarea {
              padding: 15px;
              border: none;
            }

            .flex-outer li button {
              margin-left: auto;
              padding: 8px 16px;
              border: none;
              background: #333;
              color: #f2f2f2;
              text-transform: uppercase;
              letter-spacing: .09em;
              border-radius: 2px;
            }

            .flex-inner li {
              width: 100px;
            }
          </style>
        </head>
        <body>
          <div class='container'>
            <h1>Token inválido/Expirado</h1>
          </div>
        </body>
        </html>".html_safe
      end

      def recover_html(result)
        alert = result[:msg] if result[:msg]

        html_str = "<!DOCTYPE html>
        <html>
        <head>
          <title>Vendas na mão</title>
          <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
          <style>
            body {
              font: normal 18px/1.5 'Fira Sans', 'Helvetica Neue', sans-serif;
              background: #3AAFAB;
              color: #fff;
              padding: 50px 0;
            }

            .container {
              width: 80%;
              max-width: 1200px;
              margin: 0 auto;
            }

            .container * {
              box-sizing: border-box;
            }

            .flex-outer,
            .flex-inner {
              list-style-type: none;
              padding: 0;
            }

            .flex-outer {
              max-width: 800px;
              margin: 0 auto;
            }

            .flex-outer li,
            .flex-inner {
              display: flex;
              flex-wrap: wrap;
              align-items: center;
            }

            .flex-inner {
              padding: 0 8px;
              justify-content: space-between;
            }

            .flex-outer > li:not(:last-child) {
              margin-bottom: 20px;
            }

            .flex-outer li label,
            .flex-outer li p {
              padding: 8px;
              font-weight: 300;
              letter-spacing: .09em;
              text-transform: uppercase;
            }

            .flex-outer > li > label,
            .flex-outer li p {
              flex: 1 0 120px;
              max-width: 220px;
            }

            .flex-outer > li > label + *,
            .flex-inner {
              flex: 1 0 220px;
            }

            .flex-outer li p {
              margin: 0;
            }

            .flex-outer li input:not([type='checkbox']),
            .flex-outer li textarea {
              padding: 15px;
              border: none;
            }

            .flex-outer li button {
              margin-left: auto;
              padding: 8px 16px;
              border: none;
              background: #333;
              color: #f2f2f2;
              text-transform: uppercase;
              letter-spacing: .09em;
              border-radius: 2px;
            }

            .flex-inner li {
              width: 100px;
            }
          </style>
        </head>
        <body>
          <div class='container'>
            <h1>Vendas na mão</h1>
            <h3>Recuperação de senha</h3>
            <form method='post' id='passwordForm'>
              <ul class='flex-outer'>
                <li>
                  <label for='password'>Digite sua senha</label>
                  <input type='password' name='password' id='password' placeholder='Sua senha aqui' required>
                </li>
                <li>
                  <label for='last-name'>Confirme sua senha</label>
                  <input type='password' name='password_confirmation' id='password_confirmation' placeholder='Confirme sua senha aqui' required>
                </li>
                <li>
                  <button type='submit'>Enviar</button>
                </li>
              </ul>
            </form>
          </div>
          <script src='https://code.jquery.com/jquery-1.11.1.min.js'></script>
          <script src='https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js'></script>
          <script src='https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.min.js'></script>
          <script>
            $('#passwordForm').validate({
              rules: {
                password: {
                  required: true,
                  minlength: 6
                },
                password_confirmation: {
                  required: true,
                  minlength: 6,
                  equalTo: '#password'
                },
              },
              messages: {
                password: {
                  required: 'O campo é requerido',
                  minlength: 'O campo deve ter no mínimo 6 digitos',
                },
                password_confirmation: {
                  required: 'O campo é requerido',
                  minlength: 'O campo deve ter no mínimo 6 digitos',
                  equalTo: 'Os campos não coincidem'
                },
              }
            });
          </script>"

        if alert
          html_str << "<script>alert('#{alert}')</script>"
        end

        html_str << "</body>
        </html>"

        return html_str.html_safe
      end

      def get_jwt_by_user(user, user_type = :guest)
        payload = {
          user_id: user.id,
          user_email: user.email,
          user_type: user.user_type,
          token_type: 'client_app'
        }

        if user_type == :admin
          payload[:exp] = 4.hours.from_now.to_i
        end
        jwt_encode(payload)
      end

      def fetch_social_data
        social = %w{
          facebook
          google
        }
        social_req_params = social_params

        has_social = social.index(social_req_params[:social]) if social_req_params[:social]
        if !has_social.nil? && !social_req_params[:token].nil?
          if has_social === 0
            return facebook_getdata(social_req_params[:token])
          end
          return google_getdata(social_req_params[:token])
        end

        return { error: 'Error on fetch user data' }
      end

      def password_request_params
        params.require(:auth).permit(:email, :admin)
      end

      def social_params
        params.require(:auth).permit(:social, :token)
      end

      def normal_params
        params.require(:auth).permit(:email, :password, :admin)
      end
  end
end
