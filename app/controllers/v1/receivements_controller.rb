module V1
  class ReceivementsController < ApplicationController
    before_action :set_receivement, only: [:show, :update, :destroy]

    has_scope :by_user_id
    has_scope :by_client_id
    has_scope :by_limit
    has_scope :by_offset
    has_scope :by_product_provider_id
    has_scope :like_name

    # GET /receivements
    def index
      limit = params[:by_limit]&.to_i || 50
      offset = params[:by_offset].to_i
      @receivements = apply_scopes(Receivement)
        .includes(:client)
        .by_offset(offset)
        .by_limit(limit)
        .order('receivement_date desc')

      response.set_header(
        "Total-Items",
        apply_scopes(Receivement)
          .by_offset(0)
          .unscope(:limit)
          .count
      )

      render json: @receivements, include: [:client]
    end

    # GET /receivements/1
    def show
      render json: @receivement
    end

    # POST /receivements
    def create
      @receivement = Receivement.new(receivement_params)

      if @receivement.save
        render json: @receivement, status: :created
      else
        render json: @receivement.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /receivements/1
    def update
      if @receivement.update(receivement_params)
        render json: @receivement
      else
        render json: @receivement.errors, status: :unprocessable_entity
      end
    end

    # DELETE /receivements/1
    def destroy
      @receivement.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_receivement
        @receivement = Receivement.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def receivement_params
        params.require(:receivement).permit(:receivement_date, :price, :description, :user_id, :client_id)
      end
  end
end
