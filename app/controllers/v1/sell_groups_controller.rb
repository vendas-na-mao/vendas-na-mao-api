module  V1
  class SellGroupsController < ApplicationController
    before_action :set_sell_group, only: [:show, :update, :destroy]

    has_scope :by_limit
    has_scope :by_offset
    has_scope :by_user_id
    has_scope :by_client_id

    # GET /sell_groups
    def index
      limit = params[:by_limit]&.to_i || 50
      offset = params[:by_offset].to_i
      @sell_groups = apply_scopes(SellGroup)
        .includes(:client, :sell, :product, :product_provider)
        .by_offset(offset)
        .by_limit(limit)
        .order('created_at desc')

      response.set_header(
        "Total-Items",
        apply_scopes(SellGroup)
          .by_offset(0)
          .unscope(:limit)
          .count
      )

      render json: @sell_groups, include: [:client, :sell, :product, :product_provider]
    end

    # GET /sell_groups/1
    def show
      render json: @sell_group
    end

    # POST /sell_groups
    def create
      @sell_group = SellGroup.new(sell_group_params)

      if @sell_group.save
        render json: @sell_group, status: :created
      else
        render json: @sell_group.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /sell_groups/1
    def update
      if @sell_group.update(sell_group_params)
        render json: @sell_group
      else
        render json: @sell_group.errors, status: :unprocessable_entity
      end
    end

    # DELETE /sell_groups/1
    def destroy
      @sell_group.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_sell_group
        @sell_group = SellGroup.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def sell_group_params
        params.require(:sell_group).permit(
          :user_id,
          :client_id,
          :payment_method,
          :parcels,
          sell_attributes: [
            :discount,
            :quantity,
            :discount_type,
            :delivered,
            :product_id,
            :id,
            :sell_group_id, :_destroy
          ])
      end
  end
end
