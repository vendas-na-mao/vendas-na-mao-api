module V1
  class ProductProvidersController < ApplicationController
    before_action :set_product_provider, only: [:show, :update, :destroy]

    has_scope :by_name
    has_scope :by_limit
    has_scope :by_offset
    has_scope :like_name

    # GET /product_providers
    def index
      limit = params[:by_limit]&.to_i || 50
      offset = params[:by_offset].to_i
      @product_providers = apply_scopes(ProductProvider)
        .by_offset(offset)
        .by_limit(limit)
        .order(:name)

      response.set_header(
        "Total-Items",
        apply_scopes(ProductProvider)
          .by_offset(0)
          .unscope(:limit)
          .count
      )

      render json: @product_providers
    end

    # GET /product_providers/1
    def show
      render json: @product_provider
    end

    # POST /product_providers
    def create
      @product_provider = ProductProvider.new(product_provider_params)

      if @product_provider.save
        render json: @product_provider, status: :created
      else
        render json: @product_provider.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /product_providers/1
    def update
      if @product_provider.update(product_provider_params)
        render json: @product_provider
      else
        render json: @product_provider.errors, status: :unprocessable_entity
      end
    end

    # DELETE /product_providers/1
    def destroy
      @product_provider.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_product_provider
        @product_provider = ProductProvider.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def product_provider_params
        params.require(:product_provider).permit(:name)
      end
  end
end
