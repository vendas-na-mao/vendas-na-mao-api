module V1
  class UserPreapprovalsController < ApplicationController
    before_action :set_user_preapproval, only: [:show, :update, :destroy]

    has_scope :by_limit
    has_scope :by_offset

    # GET /user_preapprovals
    def index
      limit = params[:by_limit]&.to_i || 50
      offset = params[:by_offset].to_i
      @user_preapprovals = apply_scopes(UserPreapproval)
        .includes(:user)
        .by_offset(offset)
        .by_limit(limit)

      response.set_header(
        "Total-Items",
        apply_scopes(UserPreapproval)
          .by_offset(0)
          .unscope(:limit)
          .count
      )

      render json: @user_preapprovals, include: [:user]
    end

    # GET /user_preapprovals/1
    def show
      render json: @user_preapproval
    end

    # POST /user_preapprovals
    def create
      @user_preapproval = UserPreapproval.new(user_preapproval_params)

      if @user_preapproval.save
        render json: @user_preapproval, status: :created, location: @user_preapproval
      else
        render json: @user_preapproval.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /user_preapprovals/1
    def update
      if @user_preapproval.update(user_preapproval_params)
        render json: @user_preapproval
      else
        render json: @user_preapproval.errors, status: :unprocessable_entity
      end
    end

    # DELETE /user_preapprovals/1
    def destroy
      @user_preapproval.destroy
    end

    def check
      user_id = get_user_id
      @user = User.find_by(id: user_id)
      @user_preapproval = UserPreapproval.find_by(user_id: user_id)

      limit_date = @user.created_at + 1.month
      remaining_days = (((Time.now - limit_date) / 1.day).to_i).to_s.gsub('-', '')

      response = {
        status: 200,
        data: {
          active: @user.premium?,
          days: @user_preapproval.nil? ? remaining_days  : -1,
          status: check_user_status(@user, @user_preapproval)
        }
      }

      # Goiânia, mon, 02 Sep 2018
      # According to the following requisite:
      #
      # = Colocar popup de propaganda na versão free, deixar ela livre, sempre
      #   com propaganda e na versão paga ficar o app completo sem propaganda
      #   nenhuma.
      #
      # the check below is unnececssary. The response previously built gives the
      # frontend enough info to carry on with the advertising stuff.
      #
      # TODO: remove this piece of code below, once this behaviour has been
      #       validated.

      # if (@user_preapproval.nil? && DateTime.now > (@user.created_at + 1.month)) ||
      #   (@user_preapproval && (@user_preapproval.active.nil? ||
      #   @user_preapproval.active == false) && DateTime.now > (@user.created_at + 1.month))
      #   response = {
      #     status: 401,
      #     data: {
      #       active: false,
      #       days: -1,
      #       status: check_user_status(@user, @user_preapproval)
      #     }
      #   }
      # end

      render json: response[:data], status: response[:status]
    end

    def ios_subscription
      token = request.headers['Authorization'].split
      token_decoded = jwt_decode(token.last)
      payload = token_decoded[0]

      response = {
        status: 401,
        data: {
          error: 'Erro ao ao cadastrar dado no banco de dados'
        }
      }

      if payload['user_id']
        user = User.find_by(id: payload['user_id'])
        user_preapproval = UserPreapproval.find_by(user_id: payload['user_id'])

        if user
          if user_preapproval
            user_preapproval.gateway_type = :ios
            user_preapproval.code = user_preapproval_params[:subscription_id]
            user_preapproval.active = true
            if user_preapproval.save!
              response = {
                status: 201,
                data: {
                  success: 'Cadastro realizado com sucesso'
                }
              }
            end
          else
            if UserPreapproval.create(gateway_type: :ios, code: user_preapproval_params[:subscription_id], user_id: user.id,  active: true)
              response = {
                status: 201,
                data: {
                  success: 'Cadastro realizado com sucesso'
                }
              }
            end
          end
        end
      end

      render json: response[:data], status: response[:status]
    end

    private
      def check_user_status(user, user_preapproval)
        # if (user_preapproval.nil? && DateTime.now > (user.created_at + 1.month)) ||
        #   (user_preapproval && (user_preapproval.active.nil? || user_preapproval.active == false) && DateTime.now > (user.created_at + 1.month))
        #   'expirated'
        # elsif user_preapproval && user_preapproval.active
        #   'active'
        # else
        #   'trial'
        # end

        user.premium? ? 'active' : 'trial'
      end

      def get_user_id
        token = request.headers['Authorization'].split
        token_decoded = jwt_decode(token.last)
        token_decoded[0]['user_id'] rescue nil
      end

      # Use callbacks to share common setup or constraints between actions.
      def set_user_preapproval
        @user_preapproval = UserPreapproval.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def user_preapproval_params
        params.require(:user_preapproval).permit(:type, :code, :user_id, :subscription_id)
      end
  end
end
