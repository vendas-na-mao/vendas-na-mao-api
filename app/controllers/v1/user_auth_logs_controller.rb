module V1
  class UserAuthLogsController < ApplicationController
    before_action :set_user_auth_log, only: [:show, :update, :destroy]

    has_scope :by_limit
    has_scope :by_offset

    # GET /user_auth_logs
    def index
      limit = params[:by_limit]&.to_i || 50
      offset = params[:by_offset].to_i
      @user_auth_logs = apply_scopes(UserAuthLog)
        .by_offset(offset)
        .by_limit(limit)

      response.set_header(
        "Total-Items",
        apply_scopes(UserAuthLog)
          .by_offset(0)
          .unscope(:limit)
          .count
      )

      render json: @user_auth_logs
    end

    # GET /user_auth_logs/1
    def show
      render json: @user_auth_log
    end

    # POST /user_auth_logs
    def create
      @user_auth_log = UserAuthLog.new(user_auth_log_params)

      if @user_auth_log.save
        render json: @user_auth_log, status: :created
      else
        render json: @user_auth_log.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /user_auth_logs/1
    def update
      if @user_auth_log.update(user_auth_log_params)
        render json: @user_auth_log
      else
        render json: @user_auth_log.errors, status: :unprocessable_entity
      end
    end

    # DELETE /user_auth_logs/1
    def destroy
      @user_auth_log.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_user_auth_log
        @user_auth_log = UserAuthLog.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def user_auth_log_params
        params.require(:user_auth_log).permit(:user_id)
      end
  end
end
