module V1
  class CitiesController < ApplicationController
    before_action :set_city, only: [:show, :update, :destroy]

    has_scope :by_name
    has_scope :like_name
    has_scope :by_limit
    has_scope :by_offset
    has_scope :by_state_id

    # GET /cities
    def index
      limit = params[:by_limit]&.to_i || 50
      offset = params[:by_offset].to_i
      @cities = apply_scopes(City)
        .includes(:state)
        .by_limit(limit)
        .by_offset(offset)
        .order(:name)

      response.set_header(
        "Total-Items",
        apply_scopes(City)
          .by_offset(0)
          .unscope(:limit)
          .count
      )

      render json: @cities, include: [:state]
    end

    # GET /cities/1
    def show
      render json: @city
    end

    # POST /cities
    def create
      @city = City.new(city_params)

      if @city.save
        render json: @city, status: :created
      else
        render json: @city.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /cities/1
    def update
      if @city.update(city_params)
        render json: @city
      else
        render json: @city.errors, status: :unprocessable_entity
      end
    end

    # DELETE /cities/1
    def destroy
      @city.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_city
        @city = City.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def city_params
        params.require(:city).permit(:name, :state_id)
      end
  end
end
