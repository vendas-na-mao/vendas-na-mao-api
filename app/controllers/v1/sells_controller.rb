module V1
  class SellsController < ApplicationController
    before_action :set_sell, only: [:show, :update, :destroy]

    has_scope :by_limit
    has_scope :by_offset

    # GET /sells
    def index
      limit = params[:by_limit]&.to_i || 50
      offset = params[:by_offset].to_i
      @sells = apply_scopes(Sell)
        .includes(:product, :client)
        .by_offset(offset)
        .by_limit(limit)
        .order('created_at desc')

      response.set_header(
        "Total-Items",
        apply_scopes(Sell)
          .by_offset(0)
          .unscope(:limit)
          .count
      )

      render json: @sells, include: [:product]
    end

    # GET /sells/1
    def show
      render json: @sell
    end

    # POST /sells
    def create
      @sell = Sell.new(sell_params)

      if @sell.save
        # remove 1 item on product
        Product.find_by(id: @sell.product.id)
          .update(quantity: @sell.product.quantity - @sell.quantity)

        render json: @sell, status: :created
      else
        render json: @sell.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /sells/1
    def update
      if @sell.update(sell_params)
        render json: @sell
      else
        render json: @sell.errors, status: :unprocessable_entity
      end
    end

    # DELETE /sells/1
    def destroy
      @sell.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_sell
        @sell = Sell.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def sell_params
        params.require(:sell).permit(
          :discount,
          :discount_type,
          :delivered,
          :quantity,
          :user_id,
          :product_id,
          :client_id,
          :sell_group_id
        )
      end
  end
end
