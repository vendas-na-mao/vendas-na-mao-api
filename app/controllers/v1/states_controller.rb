module V1
  class StatesController < ApplicationController
    before_action :set_state, only: [:show, :update, :destroy]

    has_scope :by_limit
    has_scope :by_offset

    # GET /states
    def index
      limit = params[:by_limit]&.to_i || 50
      offset = params[:by_offset].to_i
      @states = apply_scopes(State)
        .includes(:cities)
        .by_offset(offset)
        .by_limit(limit)
        .order(:name, 'cities.name')

      response.set_header(
        "Total-Items",
        apply_scopes(State)
          .by_offset(0)
          .unscope(:limit)
          .count
      )

      render json: @states, include: [:cities]
    end

    # GET /states/1
    def show
      render json: @state
    end

    # POST /states
    def create
      @state = State.new(state_params)

      if @state.save
        render json: @state, status: :created
      else
        render json: @state.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /states/1
    def update
      if @state.update(state_params)
        render json: @state
      else
        render json: @state.errors, status: :unprocessable_entity
      end
    end

    # DELETE /states/1
    def destroy
      @state.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_state
        @state = State.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def state_params
        params.require(:state).permit(:name)
      end
  end
end
