module V1
  class AccountsController < ApplicationController
    skip_before_action :jwt_auth_validation

    def status
      current_user.reevaluate_premium_status!

      render json: { payment_method: current_user.payment_method,
                     premium:        current_user.premium? }
    end

    def subscribe
      if current_user.update_attribute(:premium, true)
        render json: {}, status: :ok
      else
        render json: {}, status: :unprocessable_entity
      end
    end

    def unsubscribe
      if current_user.update_attribute(:premium, false)
        render json: {}, status: :ok
      else
        render json: {}, status: :unprocessable_entity
      end
    end
  end
end
