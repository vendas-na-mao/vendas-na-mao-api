module V1
  class NotificationController < ApplicationController
    include FirebaseHelper

    def send_notification
      response = send_fcm(fcm_params)
      render json: response[:data], status: response[:status]
    end

    private
      def fcm_params
        params.require(:notification).permit(:to, notification: [:title, :body])
      end
  end
end
