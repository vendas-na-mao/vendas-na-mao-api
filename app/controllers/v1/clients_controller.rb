module V1
  class ClientsController < ApplicationController
    before_action :set_client, only: [:show, :update, :destroy]

    has_scope :by_name
    has_scope :like_name
    has_scope :by_limit
    has_scope :by_offset
    has_scope :by_user_id
    has_scope :by_city_id
    has_scope :by_cpf

    # GET /clients
    def index
      limit = params[:by_limit]&.to_i || 50
      offset = params[:by_offset].to_i
      @clients = apply_scopes(Client)
        .includes(:state, :city)
        .by_limit(limit)
        .by_offset(offset)
        .order(:name)

      response.set_header(
        "Total-Items",
        apply_scopes(Client)
          .by_offset(0)
          .unscope(:limit)
          .count
      )

      render json: @clients, include: [:city, :state]
    end

    # GET /clients/1
    def show
      render json: @client
    end

    # POST /clients
    def create
      @client = Client.new(client_params)

      if @client.save
        render json: @client, status: :created
      else
        render json: @client.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /clients/1
    def update
      if @client.update(client_params)
        render json: @client
      else
        render json: @client.errors, status: :unprocessable_entity
      end
    end

    # DELETE /clients/1
    def destroy
      @client.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_client
        @client = Client.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def client_params
        params.require(:client).permit(:name, :email, :bday, :phone, :cpf, :address, :user_id, :city_id)
      end
  end
end
