module V1
  class ReportsController < ApplicationController
    include DiscountRules

    has_scope :by_limit
    has_scope :by_offset
    has_scope :by_user_id
    has_scope :by_client_id
    has_scope :like_name

    def client_cost
      limit = params[:by_limit]&.to_i || 50
      offset = params[:by_offset].to_i
      clients = apply_scopes(Client)
        .includes(:sell, :receivement, :product)
        .by_offset(offset)
        .by_limit(limit)
      client_costs = apply_client_costs_rules(clients)

      response.set_header("Total-Items", client_costs.count)

      render json: client_costs
    end

    def client_cost_by_id
      id = params[:id]&.to_i || 0
      clients = apply_scopes(Client)
        .where(id: id)
        .includes(:sell, :sell_group, :receivement, :product)
      client_costs = apply_client_costs_rules(clients, true)

      render json: !client_costs.nil? && !client_costs.empty? ? client_costs[0] : {}
    end

    def sells_and_buys_per_date
      id = params[:id]&.to_i || 0
      user_sells_and_buies_query = apply_scopes(User)
        .where(id: id)
        .includes(:bills_to_pay, :receivement, :product)
        .order('bills_to_pays.maturity_date DESC', 'receivements.receivement_date DESC')
      user_sells_and_buies = apply_user_costs_rules_per_day(user_sells_and_buies_query)

      render json: !user_sells_and_buies.nil? && !user_sells_and_buies.empty? ? user_sells_and_buies[0] : {}
    end

    def send_email
      response = {
        data: { success: 'Error' },
        status: 400,
      }

      if AuthMailer.email_test().deliver_later
        response = {
          data: { success: 'Email has been sent successful' },
          status: 200,
        }
      end

      render json: response[:data], status: response[:status]
    end
  end
end
