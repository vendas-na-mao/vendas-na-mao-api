module V1
  class AddressController < ApplicationController
    require 'uri'
    require 'net/http'
  
    def by_zip_code
      response = {
        status: 400,
        body: {
          error: 'Error! "zip code" params is missing'
        }
      }

      if params[:zip_code]
        url = URI("https://viacep.com.br/ws/#{params[:zip_code]}/json")
  
        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
  
        request = Net::HTTP::Get.new(url)
        request_response = http.request(request)
  
        request_status = 400
        request_body = {
          error: 'Error! Check if the value is valid or in correct format'
        }
        if request_response.read_body != '<h2>Bad Request (400)</h2>'
          request_status = 200
          request_body = request_response.read_body
        end
  
        response = {
          status: request_status,
          body: request_body
        }
      end
  
      render json: response[:body], status: response[:status]
    end
  end
end
