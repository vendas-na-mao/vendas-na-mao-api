module V1
  class UsersController < ApplicationController
    include PagSeguroHelper

    before_action :set_user, only: [:show, :update, :destroy, :orders]

    skip_before_action :jwt_auth_validation, only: [:create]

    has_scope :like_name
    has_scope :by_limit
    has_scope :by_offset
    has_scope :by_user_type

    # GET /users
    def index
      limit = params[:by_limit]&.to_i || 50
      offset = params[:by_offset].to_i
      users = apply_scopes(User)
        .by_offset(offset)
        .by_limit(limit)

      @users = users.map { |user| user.attributes.except('password_digest') }

      response.set_header(
        "Total-Items",
        apply_scopes(User)
          .by_offset(0)
          .unscope(:limit)
          .count
      )

      render json: @users, except: [:password_digest]
    end

    # GET /users/1
    def show
      render json: @user
    end

    # GET /users/1/orders
    def orders
      result = []
      user_preapproval = @user.user_preapproval
      if user_preapproval.length > 0
        result = get_pagseguro_preapproval_payments(user_preapproval[0].code)
      end

      render json: result
    end

    # POST /users
    def create
      @user = User.new(user_params)

      if @user.save
        render json: @user, status: :created
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /users/1
    def update
      if @user.update(user_params)
        render json: @user
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end

    # DELETE /users/1
    def destroy
      @user.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_user
        @user = User.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def user_params
        params.require(:user).permit(:name, :email, :bday, :phone, :password, :password_confirmation, :social_auth, :user_type)
      end
  end
end
