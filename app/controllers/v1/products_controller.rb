module V1
  class ProductsController < ApplicationController
    before_action :set_product, only: [:show, :update, :destroy]

    has_scope :by_user_id
    has_scope :by_limit
    has_scope :by_offset
    has_scope :like_code
    has_scope :like_description
    has_scope :exclude_by_ids
    has_scope :product_provider_id

    # GET /products
    def index
      limit = params[:by_limit]&.to_i || 50
      offset = params[:by_offset].to_i
      @products = apply_scopes(Product)
        .includes(:product_provider, :user)
        .by_offset(offset)
        .by_limit(limit)
        .order(:quantity, :code, 'created_at desc')

      response.set_header(
        "Total-Items",
        apply_scopes(Product)
          .by_offset(0)
          .unscope(:limit)
          .count
      )

      render json: @products, include: [:product_provider, :user]
    end

    # GET /products/1
    def show
      render json: @product
    end

    # POST /products
    def create
      @product = Product.new(product_params)

      if @product.save
        render json: @product, status: :created, include: [:product_provider]
      else
        render json: @product.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /products/1
    def update
      if @product.update(product_params)
        render json: @product
      else
        render json: @product.errors, status: :unprocessable_entity
      end
    end

    # DELETE /products/1
    def destroy
      @product.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_product
        @product = Product.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def product_params
        params.require(:product).permit(:code, :description, :sell_price, :buy_price, :product_provider_id, :user_id, :quantity)
      end
  end
end
