module V1
  class BillsToPaysController < ApplicationController
    before_action :set_bills_to_pay, only: [:show, :update, :destroy]

    has_scope :by_product_provider_id
    has_scope :by_limit
    has_scope :by_offset
    has_scope :by_user_id
    has_scope :like_name

    # GET /bills_to_pays
    def index
      limit = params[:by_limit]&.to_i || 50
      offset = params[:by_offset].to_i
      @bills_to_pays = apply_scopes(BillsToPay)
        .includes(:product_provider)
        .by_offset(offset)
        .by_limit(limit)
        .order(:paid_out, 'maturity_date desc')

      response.set_header(
        "Total-Items",
        apply_scopes(BillsToPay)
          .by_offset(0)
          .unscope(:limit)
          .count
      )

      render json: @bills_to_pays, include: [:product_provider]
    end

    # GET /bills_to_pays/1
    def show
      render json: @bills_to_pay
    end

    # POST /bills_to_pays
    def create
      @bills_to_pay = BillsToPay.new(bills_to_pay_params)

      if @bills_to_pay.save
        render json: @bills_to_pay, status: :created
      else
        render json: @bills_to_pay.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /bills_to_pays/1
    def update
      if @bills_to_pay.update(bills_to_pay_params)
        render json: @bills_to_pay
      else
        render json: @bills_to_pay.errors, status: :unprocessable_entity
      end
    end

    # DELETE /bills_to_pays/1
    def destroy
      @bills_to_pay.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_bills_to_pay
        @bills_to_pay = BillsToPay.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def bills_to_pay_params
        params.require(:bills_to_pay).permit(:maturity_date, :price, :user_id, :paid_out, :product_provider_id)
      end
  end
end
