module V1
  class PagseguroController < ApplicationController
    skip_before_action :jwt_auth_validation, only: [:notifications]

    include PagSeguroHelper

    def preapprovals
      render json: get_pagseguro_preapprovals
    end

    def preapproval_payments
      pre_approval = UserPreapproval.find_by(user_id: get_user_id)
      if pre_approval
        response = get_pagseguro_preapproval_payments(pre_approval.code, params[:page] || 1)
        status = if response.is_a?(Hash) && !response['error'].nil? && response['error']
          400
        else
          200
        end
        render json: response, status: status
      else
        render json: { error: 'Invalid token' }, status: 401
      end
    end

    def get_session
      render json: get_pagseguro_session
    end

    def subscribe_plan
      @user_id = get_user_id
      user_preapproval = UserPreapproval.find_by(user_id: @user_id)
      if user_preapproval.nil?
        result = create_pagseguro_subscription(subscribe_plan_data)
      else
        # get preapproval
        preapproval = get_pagseguro_preapproval(user_preapproval.code)

        if preapproval['status'] == 'CANCELLED' || preapproval['status'] == 'SUSPENDED'
          user_preapproval.delete

          result = create_pagseguro_subscription(subscribe_plan_data)
        elsif preapproval['status'] == 'ACTIVE' || preapproval['status'] == 'PENDING'
          result = {
            status: 400,
            data: { error: 'Sua assinatura se encontra ativa ou pendente no momento, favor acessar o painel do Pagseguro' }
          }
        else
          result = {
            status: 400,
            data: { error: 'Erro inesperado ao fazer assinatura, favor tente novamente mais tarde' }
          }
        end
      end

      render json: result[:data], status: result[:status]
    end

    def check_plan_subscription
      @user_id = get_user_id
      data = check_plan_data

      user_preapproval = UserPreapproval.find_by(user_id: @user_id, code: data[:code], active: true)

      if user_preapproval
        render json: { valid_subscription: true }, status: 200
      else
        render json: { valid_subscription: false }, status: 401
      end
    end

    def notifications
      if params[:notificationType] == 'preApproval'
        result = get_pagseguro_preapproval_notification(params[:notificationCode])
        user_preapproval = UserPreapproval.find_by(code: result['code'])
        if user_preapproval
          if !user_preapproval.tracker
            user_preapproval.tracker = result['tracker']
          end

          user_preapproval.active = result['status'] === 'ACTIVE' ? true : false
          user_preapproval.save and user_preapproval.touch
        end
      end
    end

    private
      def create_pagseguro_subscription(data)
        response = subscribe_pagseguro_plan(data)
        if response['code']
          user_preapproval = UserPreapproval.new(gateway_type: 0, code: response['code'], user_id: @user_id, active: false)
          if user_preapproval.save
            return {
              status: :created,
              data: user_preapproval
            }
          else
            return {
              status: :unprocessable_entity,
              data: user_preapproval.errors
            }
          end
        elsif (!response['error'].nil? && response['error'] == true) && response['errors']
          return {
            status: 400,
            data: {
              error: get_pagseguro_errors(response['errors'])
            }
          }
        else
          return {
            status: 400,
            data: { error: 'Erro ao fazer assinatura, favor tente novamente mais tarde' }
          }
        end
      end

      def get_user_id
        token = request.headers['Authorization'].split
        token_decoded = jwt_decode(token.last)
        @user_id = token_decoded[0]['user_id'] rescue nil
      end

      def subscribe_plan_data
        params.require(:pagseguro)
      end

      def check_plan_data
        params.require(:pagseguro).permit(:code)
      end
  end
end
