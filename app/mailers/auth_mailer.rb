class AuthMailer < ApplicationMailer

  def password_recovery(host, email, name, token)
    @name = name
    @email = email
    @host  = host
    @token  = token
    mail(to: email, subject: '[Vendas na Mão] Recuperação de senha')
  end

  def email_test
    mail(
      to: 'diegomelo182@gmail.com',
      subject: '[Vendas na Mão] Teste'
    )
  end

end
