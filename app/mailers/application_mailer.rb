class ApplicationMailer < ActionMailer::Base
  default from: 'diegomelo182@gmail.com'
  layout 'mailer'
end
