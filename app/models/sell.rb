# == Schema Information
#
# Table name: sells
#
#  id            :bigint(8)        not null, primary key
#  discount      :decimal(8, 2)
#  quantity      :integer
#  discount_type :integer
#  delivered     :boolean
#  sell_group_id :bigint(8)
#  product_id    :bigint(8)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  deleted_at    :datetime
#

class Sell < ApplicationRecord
  acts_as_paranoid

  enum discount_type: {
    percentage: 0,
    price: 1
  }

  belongs_to :product, inverse_of: :sell
  belongs_to :sell_group, inverse_of: :sell

  has_many :client, through: :sell_group, inverse_of: :sell

  validates :quantity,
            presence: true,
            numericality: {
              greater_than_or_equal_to: 1
            }

  validates :product_id,
            presence: true,
            numericality: true

  validates :discount,
            allow_blank: true,
            presence: true,
            numericality: true

  validates :discount_type,
            presence: true

  validate :discount_validation

  def final_value
    raw_value = product.sell_price * quantity
    apply_discount(raw_value)
  end

  private

  def apply_discount(value)
    return value if discount.blank? || discount.zero?

    if percentage?
      value - ((discount / 100.0) * value)
    else
      value - discount
    end
  end

  def discount_validation
    return if discount.blank? || discount_type.blank?

    case discount_type
    when 'percentage' then validate_discount_in_percentage
    when 'price'      then validate_discount_in_value
    end
  end

  def validate_discount_in_percentage
    return if discount.between?(1, 100)
    errors.add(:discount, 'O valor do desconto deve estar entre 1% e 100%')
  end

  def validate_discount_in_value
    invalid_by_sell_price = product.sell_price.present? &&
                            product.sell_price != 0 &&
                            product.sell_price < discount
    invalid_by_buy_price  = product.buy_price.present? &&
                            product.buy_price != 0 &&
                            product.buy_price < discount
    return unless invalid_by_sell_price || invalid_by_buy_price

    errors.add(:discount,
               'O valor do desconto deve ser menor que o valor do produto')
  end
end
