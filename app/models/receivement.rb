# == Schema Information
#
# Table name: receivements
#
#  id               :bigint(8)        not null, primary key
#  receivement_date :date
#  price            :decimal(8, 2)
#  user_id          :bigint(8)
#  client_id        :bigint(8)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  deleted_at       :datetime
#

class Receivement < ApplicationRecord
  acts_as_paranoid

  scope :by_user_id, -> (user_id) {
    where(user_id: user_id)
  }

  scope :by_client_id, -> (client_id) {
    where(client_id: client_id)
  }
  # scope :by_product_provider_id,
  #   -> product_provider_id {
  #     where(product_provider_id: product_provider_id)
  #   }

  scope :like_name, -> (client_name) {
    joins(:client).merge(Client.where('clients.name ILIKE ?', "%#{client_name}%"))
  }

  belongs_to :user, inverse_of: :receivement
  belongs_to :client, inverse_of: :receivement
  # belongs_to :product_provider, inverse_of: :receivement

  validates :receivement_date,
    presence: true,
    format: {
      with: /\A([0-9]{4}\-[0-9]{2}\-[0-9]{2})|([0-9]{2}\/[0-9]{2}\/[0-9]{4})\z/
    }

  validates :price,
    presence: true,
    numericality: true

  validates :user_id,
    presence: true,
    numericality: true

  validates :client_id,
    presence: true,
    numericality: true
end
