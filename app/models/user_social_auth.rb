# == Schema Information
#
# Table name: user_social_auths
#
#  id             :bigint(8)        not null, primary key
#  social_user_id :string(260)
#  social_media   :integer
#  user_id        :bigint(8)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class UserSocialAuth < ApplicationRecord
  enum social_media: [:facebook, :google]

  belongs_to :user, inverse_of: :user_social_auth

  validates :social_user_id, :social_media,
    presence: true

end
