class ApplicationRecord < ActiveRecord::Base
  include DefaultScopes

  self.abstract_class = true
end
