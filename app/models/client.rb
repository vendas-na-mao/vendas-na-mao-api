# == Schema Information
#
# Table name: clients
#
#  id         :bigint(8)        not null, primary key
#  name       :text
#  bday       :date
#  email      :text
#  phone      :string(200)
#  cpf        :string(200)
#  address    :text
#  user_id    :bigint(8)
#  city_id    :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#

class Client < ApplicationRecord
  acts_as_paranoid

  scope :by_user_id, -> user_id { where(user_id: user_id) }
  scope :by_city_id, -> city_id { where(city_id: city_id) }
  scope :by_cpf, -> cpf { where(cpf: cpf) }

  belongs_to :user
  belongs_to :city

  has_one :state, through: :city, inverse_of: :client
  has_many :sell_group, dependent: :destroy, inverse_of: :client
  has_many :receivement, dependent: :destroy, inverse_of: :client
  has_many :sell, through: :sell_group, inverse_of: :client
  has_many :product, through: :sell, inverse_of: :client
  has_many :product_provider, through: :product, inverse_of: :client

  # validate :bday_not_be_in_the_future

  # validates_with CpfValidator

  validates :name,
    presence: true

  validates :phone,
    presence: true,
    format: {
      with: /\A((\([0-9]{2}\))|([0-9]{2}))\s?[0-9]?[0-9]{4}\-?[0-9]{4}\z/
    },
    length: {
      minimum: 10,
      maximum: 15
    }

  validates :email,
    allow_nil: true,
    allow_blank: true,
    uniqueness: true,
    format: {
      with: /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
    }

  # validates :cpf,
  #   presence: true,
  #   uniqueness: true

  # validates :bday,
  #   presence: true

  validates :user_id,
    presence: true,
    numericality: true

  validates :city_id,
    presence: true,
    numericality: true

  private
    def bday_not_be_in_the_future
      if bday.present? && bday > Date.today
        errors.add(:bday, "Não pode ser no futuro")
      end
    end
end
