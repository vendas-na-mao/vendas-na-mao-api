# == Schema Information
#
# Table name: user_auth_logs
#
#  id         :bigint(8)        not null, primary key
#  user_id    :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UserAuthLog < ApplicationRecord
  belongs_to :user

  validates :user_id,
    presence: true,
    numericality: { only_integer: true }

end
