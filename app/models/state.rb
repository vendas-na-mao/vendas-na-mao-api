# == Schema Information
#
# Table name: states
#
#  id         :bigint(8)        not null, primary key
#  name       :string(260)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#

class State < ApplicationRecord
  acts_as_paranoid

  has_many :cities, inverse_of: :state
  has_many :client, inverse_of: :state
end
