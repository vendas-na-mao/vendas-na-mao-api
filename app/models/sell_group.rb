# == Schema Information
#
# Table name: sell_groups
#
#  id             :bigint(8)        not null, primary key
#  user_id        :bigint(8)
#  client_id      :bigint(8)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  deleted_at     :datetime
#  payment_method :integer          default("cash")
#  parcels        :integer
#

class SellGroup < ApplicationRecord
  acts_as_paranoid

  # The translation is quite ugly, so I wrote here the original names:
  enum payment_method: {
    cash: 0,                # À vista
    credit_card_in_one: 1,  # Cartão à vista (disgusting, I know...)
    credit_card_parcels: 2, # Cartão parcelado
    other_parcels: 3,       # Parcelamento próprio
  }

  scope :by_user_id, ->(user_id) { where(user_id: user_id) }
  scope :by_client_id, ->(client_id) { where(client_id: client_id) }

  before_save :product_quantity_verification_before_create
  after_save :product_quantity_verification_after_create
  after_create :schedule_receivments
  before_update :product_quantity_verification_on_update
  before_destroy :product_quantity_verification_on_destroy

  belongs_to :user, inverse_of: :sell_group
  belongs_to :client, inverse_of: :sell_group

  has_many :sell, dependent: :destroy, inverse_of: :sell_group
  has_many :product, through: :sell
  has_many :product_provider, through: :product
  has_many :receivements, through: :client

  accepts_nested_attributes_for :sell, allow_destroy: true

  alias_attribute :sells, :sell

  validates :user_id,
            presence: true,
            numericality: true

  validates :client_id,
            presence: true,
            numericality: true

  validate :parcels_validation

  private

  def parcels_validation
    return unless (credit_card_parcels? || other_parcels?) && parcels <= 1
    errors.add(:parcels, 'A quantidade de parcelas deve ser maior que 1')
  end

  def schedule_receivments
    total_to_receive = sells.sum(&:final_value)
    parcel_to_receive, parcel_count = if cash? || credit_card_in_one?
                                        [total_to_receive, 1]
                                      else
                                        [(total_to_receive / parcels), parcels]
                                      end

    parcel_count.times do |count|
      Receivement.create(receivement_date: Date.current + count.months,
                         price: parcel_to_receive,
                         client_id: client_id,
                         user_id: user_id)
    end
  end

  # From this point below, quoting JUNIOR, Neymar:
  # "Vai tomar no cu. Don't touch!"
  #
  # Ps.: This is a message to present me, not to anyone in the future. Feel free
  #      to explore the wild jungle ahead of you.
  def product_quantity_verification_before_create
    if valid? && !sell.nil? && !sell.empty?
      sell.each do |item|
        actualSell = Sell.find_by(id: item.id)

        if actualSell.nil?
          quantity = item.product.quantity - item.quantity
          Product.find_by(id: item.product.id)
          .update(quantity: quantity)
        end
      end
    end
  end

  def product_quantity_verification_after_create
    if valid? && !sell.nil? && !sell.empty?
      sell.each do |item|
        actualSell = Sell.find_by(id: item.id)

        if actualSell.nil?
          quantity = item.product.quantity - item.quantity
          Product.find_by(id: item.product.id)
          .update(quantity: quantity)
        end
      end
    end
  end

  def product_quantity_verification_on_update
    if valid? && !sell.nil? && !sell.empty?
      sell.each do |item|
        actualSell = Sell.find_by(id: item.id)

        if !item.product.nil? && !actualSell.nil? &&
          (item.quantity != actualSell.quantity)

          if (item.quantity < actualSell.quantity) ||
            (item._destroy.present? || item._destroy == '1')

            addQuantity = actualSell.quantity - item.quantity
            if item._destroy.present?
              addQuantity = item.quantity
            end
            Product.find_by(id: item.product.id)
              .update(quantity: item.product.quantity + addQuantity)

          elsif (item.quantity > actualSell.quantity)

            addQuantity = item.quantity - actualSell.quantity
            quantity = item.product.quantity - addQuantity
            Product.find_by(id: item.product.id)
              .update(quantity: quantity)
          end

        elsif item._destroy.present?
          Product.find_by(id: item.product.id)
            .update(quantity: item.product.quantity + item.quantity)
        end
      end
    end
  end

  def product_quantity_verification_on_destroy
    if !sell.nil? && !sell.empty?
      sell.each do |item|
        if !item.delivered || !item.delivered && item._destroy.present?
          Product.find_by(id: item.product.id)
            .update(quantity: item.product.quantity + item.quantity)
        end
      end
    end
  end
end
