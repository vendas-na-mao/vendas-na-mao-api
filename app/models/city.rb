# == Schema Information
#
# Table name: cities
#
#  id         :bigint(8)        not null, primary key
#  name       :text
#  state_id   :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#

class City < ApplicationRecord
  acts_as_paranoid

  scope :by_state_id,
    -> (state_id){
      where(state_id: state_id)
    }

  belongs_to :state, inverse_of: :cities
end
