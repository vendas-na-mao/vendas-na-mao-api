# == Schema Information
#
# Table name: users
#
#  id              :bigint(8)        not null, primary key
#  name            :text
#  email           :text
#  bday            :date
#  phone           :string(260)
#  user_type       :integer
#  password_digest :string(260)
#  social_auth     :boolean
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  deleted_at      :datetime
#  premium         :boolean          default(FALSE), not null
#

class User < ApplicationRecord
  acts_as_paranoid

  has_secure_password

  scope :by_user_type, ->(user_type) { where(user_type: user_type) }

  enum user_type: {
    admin: 0,
    guest: 1
  }

  has_one :user_social_auth, inverse_of: :user, dependent: :destroy

  has_many :bills_to_pay, dependent: :destroy
  has_many :receivement, dependent: :destroy
  has_many :sell_group, dependent: :destroy
  has_many :user_preapproval, dependent: :destroy
  has_many :sell, through: :sell_group, dependent: :destroy
  has_many :product, through: :sell
  has_many :clients

  accepts_nested_attributes_for :user_social_auth, allow_destroy: true

  validate :bday_not_be_in_the_future

  validates :name,
            presence: true,
            length: {
              minimum: 3,
              maximum: 500
            }

  validates :bday,
            allow_nil: true,
            format: {
              with: %r[\A(\d{4}\-\d{2}\-\d{2})|(\d{2}\/\d{2}\/\d{4})\z]
            }

  validates :phone,
            presence: true,
            format: {
              with: /\A((\(\d{2}\))|(\d{2}))\s?\d?\d{4}\-?\d{4}\z/,
              message: 'Favor verifique o formato do campo. Ex: (16) 98965-7864'
            },
            length: {
              minimum: 10,
              maximum: 15
            },
            if: proc { |a| !a.social_auth? }

  validates :email,
            uniqueness: true,
            presence: true,
            format: {
              with: /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i,
              message: 'Favor verifique o formato do campo. ' \
                       'Ex: joaodasilva@gmail.com'
            }

  validates :password,
            length: {
              minimum: 6,
              maximum: 24
            },
            presence: true,
            if: proc { |a| a.social_auth === 0 }

  validates :password_confirmation,
            length: {
              minimum: 6,
              maximum: 24
            },
            presence: true,
            if: proc { |a| a.social_auth === 0 }

  validates :user_type, presence: true

  # [código-legado]
  # De quando se usava o pagseguro como método de pagamento
  def preapproval_status
    if user_preapproval.empty?
      created_at < 1.month.ago ? 'trial expirado' : 'trial'
    else
      user_preapproval.first.active? ? 'ativo' : 'inativo'
    end
  end
  # [/código-legado]

  def payment_method
    return unless premium?

    user_preapproval.active.any? ? :pagseguro : :iap
  end

  def reevaluate_premium_status!
    return unless premium? && payment_method == :pagseguro

    preapproval    = user_preapproval.active.first
    still_premium  = preapproval.present?
    still_premium &= Date.current < (preapproval.updated_at + 1.month)

    return if still_premium

    update_attribute(:premium, false)
    preapproval.update_attribute(:active, false)
  end

  private

  def bday_not_be_in_the_future
    return if bday.blank?

    errors.add(:bday, 'Não pode ser no futuro') if bday > Date.today
  end
end
