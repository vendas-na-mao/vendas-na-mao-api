# == Schema Information
#
# Table name: product_providers
#
#  id         :bigint(8)        not null, primary key
#  name       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  deleted_at :datetime
#

class ProductProvider < ApplicationRecord
  acts_as_paranoid

  has_many :product, inverse_of: :product_provider
  has_many :products, inverse_of: :product_provider
  has_many :bills_to_pay, inverse_of: :product_provider
  has_many :receivement, inverse_of: :product_provider

  validate :check_uniquess_name

  validates :name,
    presence: true

  private
    def check_uniquess_name
      if name.present?
        p_provider = ProductProvider.where('unaccent(name) = ?', I18n.transliterate(name))

        if !p_provider.nil? && p_provider.length > 0
          errors.add(:name, "Já existe um fornecedor com o mesmo nome, favor usar outro nome.")
        end
      end
    end

end
