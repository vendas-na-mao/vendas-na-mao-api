class CpfValidator < ActiveModel::Validator
  def validate(record)
    cpf_validation = check_cpf(record.cpf)

    unless cpf_validation
      record.errors.add(:cpf_invalid, "invalid cpf")
    end
  end

  private
  def check_cpf(cpf=nil)
    return false if cpf.nil?
  
    winvalid = %w{12345678909 11111111111 22222222222 33333333333 44444444444 55555555555 66666666666 77777777777 88888888888 99999999999 00000000000}
    wvalue = cpf.scan /[0-9]/
    if wvalue.length == 11
      unless winvalid.member?(wvalue.join)
        wvalue = wvalue.collect{|x| x.to_i}
        wsum = 10*wvalue[0]+9*wvalue[1]+8*wvalue[2]+7*wvalue[3]+6*wvalue[4]+5*wvalue[5]+4*wvalue[6]+3*wvalue[7]+2*wvalue[8]
        wsum = wsum - (11 * (wsum/11))
        wresult1 = (wsum == 0 or wsum == 1) ? 0 : 11 - wsum
        if wresult1 == wvalue[9]
          wsum = wvalue[0]*11+wvalue[1]*10+wvalue[2]*9+wvalue[3]*8+wvalue[4]*7+wvalue[5]*6+wvalue[6]*5+wvalue[7]*4+wvalue[8]*3+wvalue[9]*2
          wsum = wsum - (11 * (wsum/11))
          wresult2 = (wsum == 0 or wsum == 1) ? 0 : 11 - wsum
          return true if wresult2 == wvalue[10] # CPF validado
        end
      end
    end
    return false # CPF invalidado
  end
end