module DefaultScopes
  require "i18n"
  extend ActiveSupport::Concern

  included do
    scope :like_name, -> (name) { where('unaccent(name) ILIKE ?', "%#{I18n.transliterate(name)}%") }
    scope :by_name, -> (name) { where(name: name) }
    scope :by_limit, -> (param) { limit(param || 50) }
    scope :by_offset, -> (param) { offset(param || 0) }
  end

end