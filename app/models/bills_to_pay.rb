# == Schema Information
#
# Table name: bills_to_pays
#
#  id                  :bigint(8)        not null, primary key
#  maturity_date       :date
#  price               :decimal(8, 2)
#  paid_out            :boolean
#  user_id             :bigint(8)
#  product_provider_id :bigint(8)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  deleted_at          :datetime
#

class BillsToPay < ApplicationRecord
  acts_as_paranoid

  scope :by_product_provider_id,
    -> (product_provider_id) {
      where(product_provider_id: product_provider_id)
    }
  scope :by_user_id, -> user_id { where(user_id: user_id) }
  scope :like_name, -> product_provider_name {
    joins(:product_provider).merge(ProductProvider.where("name ILIKE ?", "%#{product_provider_name}%"))
  }

  belongs_to :user, inverse_of: :bills_to_pay
  belongs_to :product_provider, inverse_of: :bills_to_pay

  validates :maturity_date,
    presence: true,
    format: {
      with: /\A([0-9]{4}\-[0-9]{2}\-[0-9]{2})|([0-9]{2}\/[0-9]{2}\/[0-9]{4})\z/
    }

  validates :price,
    presence: true,
    numericality: true

  validates :product_provider_id,
    presence: true,
    numericality: true

  validates :user_id,
    presence: true,
    numericality: true

end
