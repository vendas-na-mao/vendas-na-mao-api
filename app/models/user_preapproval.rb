# == Schema Information
#
# Table name: user_preapprovals
#
#  id           :bigint(8)        not null, primary key
#  gateway_type :integer
#  code         :string
#  tracker      :string
#  active       :boolean
#  user_id      :bigint(8)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  deleted_at   :datetime
#

class UserPreapproval < ApplicationRecord
  acts_as_paranoid

  enum gateway_type: [
    :pagseguro,
    :ios
  ]

  belongs_to :user, inverse_of: :user_preapproval

  validates :gateway_type,
    presence: true

  validates :code,
    presence: true

  validates :user_id,
    presence: true,
    numericality: true,
    uniqueness: true

  scope :active, -> { where(active: true) }
end
