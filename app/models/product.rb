# == Schema Information
#
# Table name: products
#
#  id                  :bigint(8)        not null, primary key
#  description         :text
#  code                :text
#  buy_price           :decimal(8, 2)
#  sell_price          :decimal(8, 2)    default(0.0)
#  quantity            :integer
#  user_id             :bigint(8)
#  product_provider_id :bigint(8)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  deleted_at          :datetime
#

class Product < ApplicationRecord
  acts_as_paranoid

  scope :product_provider_id, -> (product_provider_id) {
    where(product_provider_id: product_provider_id)
  }

  scope :by_user_id,
    -> (user_id) {
      where(user_id: user_id)
    }

  scope :like_code,
    -> (code) {
      where("code ILIKE ?", "%#{code}%")
    }

  scope :exclude_by_ids,
    -> (ids) {
      where.not(id: ids.split(','))
    }

  scope :like_description,
    -> (description) {
      where("unaccent(description) ILIKE ?", "%#{I18n.transliterate(description)}%")
    }

  belongs_to :product_provider, inverse_of: :product
  belongs_to :user, inverse_of: :product

  has_many :client, inverse_of: :product
  has_many :sell, dependent: :destroy

  validate :check_if_sell_price_is_greater_then_buy_price

  accepts_nested_attributes_for :sell

  validates :description,
    presence: true

  validates :code,
    uniqueness: {
      scope: :user_id
    }

  validates :product_provider_id,
    presence: true,
    numericality: true

  validates :buy_price,
    presence: true,
    numericality: true

  validates :sell_price,
    numericality: {
      allow_blank: true
    }

  validates :quantity,
    presence: true,
    numericality: true

  private
    def check_if_sell_price_is_greater_then_buy_price
      if (buy_price.present? && buy_price > 0 && sell_price.present? && sell_price > 0)  && sell_price < buy_price
        errors.add(:buy_price, "O valor de venda deve ser maior que o valor de compra")
      end
    end

end
