FROM ruby:2.4.1
WORKDIR /vendas-na-mao
ADD Gemfile /vendas-na-mao/Gemfile
ADD Gemfile.lock /vendas-na-mao/Gemfile.lock
ADD . /vendas-na-mao
RUN apt-get update -qq &&\
    apt-get install -y build-essential libpq-dev nodejs &&\
    apt-get clean &&\
    bundle install