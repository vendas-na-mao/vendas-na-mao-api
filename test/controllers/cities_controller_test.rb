require 'test_helper'

class CitiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @city = cities(:one)
  end

  test "should get index" do
    get v1_cities_url, as: :json
    assert_response :success
  end

  test "should show city" do
    get v1_city_url(@city), as: :json
    assert_response :success
  end
end
