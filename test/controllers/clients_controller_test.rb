require 'test_helper'

class ClientsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @client = clients(:one)
  end

  test "should get index" do
    get v1_clients_url, as: :json
    assert_response :success
  end

  test "should create client" do
    assert_difference('Client.count') do
      post v1_clients_url, params: { client: { address: @client.address, bday: @client.bday, city_id: @client.city_id, cpf: @client.cpf, name: @client.name, phone: @client.phone, user_id: @client.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show client" do
    get v1_client_url(@client), as: :json
    assert_response :success
  end

  test "should update client" do
    patch v1_client_url(@client), params: {
      client: {
        address: @client.address,
        bday: @client.bday,
        city_id: @client.city_id,
        cpf: @client.cpf,
        name: @client.name,
        phone: @client.phone,
        user_id: @client.user_id
      }
    }, as: :json
    assert_response 200
  end

  test "should destroy client" do
    assert_difference('Client.count', -1) do
      delete v1_client_url(@client), as: :json
    end

    assert_response 204
  end
end
