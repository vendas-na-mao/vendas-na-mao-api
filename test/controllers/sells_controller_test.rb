require 'test_helper'

class SellsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sell = sells(:one)
    @sg   = sell_groups(:one)
  end

  test "should get index" do
    get v1_sells_url, as: :json
    assert_response :success
  end

  test "should create sell" do
    assert_difference('Sell.count') do
      post v1_sells_url, params: {
        sell: {
          delivered:     @sell.delivered,
          discount:      @sell.discount,
          discount_type: @sell.discount_type,
          product_id:    @sell.product_id,
          quantity:      @sell.quantity,
          sell_group_id: @sg.id
        }
      }, as: :json
    end

    assert_response 201
  end

  test "should show sell" do
    get v1_sell_url(@sell), as: :json
    assert_response :success
  end

  test "should update sell" do
    patch v1_sell_url(@sell), params: {
      sell: {
        delivered: @sell.delivered,
        discount: @sell.discount,
        discount_type: @sell.discount_type,
        product_id: @sell.product_id,
        quantity: @sell.quantity,
        sell_group_id: @sell.sell_group_id
      }
    }, as: :json
    assert_response 200
  end

  test "should destroy sell" do
    assert_difference('Sell.count', -1) do
      delete v1_sell_url(@sell), as: :json
    end

    assert_response 204
  end
end
