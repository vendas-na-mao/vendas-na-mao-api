require 'test_helper'

class ProductProvidersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product_provider = product_providers(:one)
  end

  test "should get index" do
    get v1_product_providers_url, as: :json
    assert_response :success
  end

  test "should create product_provider" do
    assert_difference('ProductProvider.count') do
      post v1_product_providers_url, params: {
        product_provider: {
          name: 'Suave Fragrance'
        }
      }, as: :json
    end

    assert_response 201
  end

  test "should show product_provider" do
    get v1_product_provider_url(@product_provider), as: :json
    assert_response :success
  end

  test "should update product_provider" do
    patch v1_product_provider_url(@product_provider), params: {
      product_provider: {
        name: 'Suave Fragrance'
      }
    }, as: :json
    assert_response 200
  end

  test "should destroy product_provider" do
    assert_difference('ProductProvider.count', -1) do
      delete v1_product_provider_url(@product_provider), as: :json
    end

    assert_response 204
  end
end
