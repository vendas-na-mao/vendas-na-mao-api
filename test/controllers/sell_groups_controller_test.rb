require 'test_helper'

class SellGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sell_group = sell_groups(:one)
  end

  test "should get index" do
    get v1_sell_groups_url, as: :json
    assert_response :success
  end

  test "should create sell_group" do
    assert_difference('SellGroup.count') do
      post v1_sell_groups_url, params: { sell_group: { client_id: @sell_group.client_id, user_id: @sell_group.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should create receivements" do
    assert_difference('Receivement.count') do
      post v1_sell_groups_url, params: { sell_group: { client_id: @sell_group.client_id, user_id: @sell_group.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show sell_group" do
    get v1_sell_group_url(@sell_group), as: :json
    assert_response :success
  end

  test "should update sell_group" do
    patch v1_sell_group_url(@sell_group), params: { sell_group: { client_id: @sell_group.client_id, user_id: @sell_group.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy sell_group" do
    assert_difference('SellGroup.count', -1) do
      delete v1_sell_group_url(@sell_group), as: :json
    end

    assert_response 204
  end
end
