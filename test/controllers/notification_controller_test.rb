require 'test_helper'

class NotificationControllerTest < ActionDispatch::IntegrationTest
  test "should get send" do
    post send_notification_v1_notification_index_url, params: {
      notification: {
        to: 'me',
        notification: {
          title: 'title',
          body: 'body'
        }
      }
    }
    assert_response :success
  end
end
