require 'test_helper'

class StatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @state = states(:one)
  end

  test "should get index" do
    get v1_states_url, as: :json
    assert_response :success
  end

  test "should show state" do
    get v1_state_url(@state), as: :json
    assert_response :success
  end
end
