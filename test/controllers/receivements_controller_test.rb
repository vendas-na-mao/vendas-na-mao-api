require 'test_helper'

class ReceivementsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @receivement = receivements(:one)
  end

  test "should get index" do
    get v1_receivements_url, as: :json
    assert_response :success
  end

  test "should create receivement" do
    assert_difference('Receivement.count') do
      post v1_receivements_url, params: {
        receivement: {
          client_id: @receivement.client_id,
          price: @receivement.price,
          receivement_date: @receivement.receivement_date,
          user_id: @receivement.user_id
        }
      }, as: :json
    end

    assert_response 201
  end

  test "should show receivement" do
    get v1_receivement_url(@receivement), as: :json
    assert_response :success
  end

  test "should update receivement" do
    patch v1_receivement_url(@receivement), params: {
      receivement: {
        client_id: @receivement.client_id,
        price: @receivement.price,
        receivement_date: @receivement.receivement_date,
        user_id: @receivement.user_id
      }
    }, as: :json
    assert_response 200
  end

  test "should destroy receivement" do
    assert_difference('Receivement.count', -1) do
      delete v1_receivement_url(@receivement), as: :json
    end

    assert_response 204
  end
end
