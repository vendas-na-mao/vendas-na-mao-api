require 'test_helper'

class BillsToPaysControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bills_to_pay = bills_to_pays(:one)
  end

  test "should get index" do
    get v1_bills_to_pays_url, as: :json
    assert_response :success
  end

  test "should create bills_to_pay" do
    assert_difference('BillsToPay.count') do
      post v1_bills_to_pays_url, params: { bills_to_pay: { maturity_date: @bills_to_pay.maturity_date, price: @bills_to_pay.price, product_provider_id: @bills_to_pay.product_provider_id, user_id: @bills_to_pay.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show bills_to_pay" do
    get v1_bills_to_pay_url(@bills_to_pay), as: :json
    assert_response :success
  end

  test "should update bills_to_pay" do
    patch v1_bills_to_pay_url(@bills_to_pay), params: { bills_to_pay: { maturity_date: @bills_to_pay.maturity_date, price: @bills_to_pay.price, product_provider_id: @bills_to_pay.product_provider_id, user_id: @bills_to_pay.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy bills_to_pay" do
    assert_difference('BillsToPay.count', -1) do
      delete v1_bills_to_pay_url(@bills_to_pay), as: :json
    end

    assert_response 204
  end
end
