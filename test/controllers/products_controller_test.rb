require 'test_helper'

class ProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product = products(:one)
    @product2 = products(:two)
  end

  test "should get index" do
    get v1_products_url, as: :json
    assert_response :success
  end

  test "should create product" do
    assert_difference('Product.count') do
      post v1_products_url, params: {
        product: {
          buy_price: @product.buy_price,
          code: 'Mike caráia me deixa criar',
          description: @product.description,
          product_provider_id: @product.product_provider_id,
          quantity: @product.quantity,
          sell_price: @product.sell_price,
          user_id: @product.user_id
        }
      }, as: :json
    end

    assert_response 201
  end

  test "should show product" do
    get v1_product_url(@product), as: :json
    assert_response :success
  end

  test "should update product" do
    patch v1_product_url(@product), params: {
      product: {
        description: @product2.description,
      }
    }, as: :json
    assert_response 200
  end

  test "should destroy product" do
    assert_difference('Product.count', -1) do
      delete v1_product_url(@product), as: :json
    end

    assert_response 204
  end
end
