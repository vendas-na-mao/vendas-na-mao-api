set :branch, 'staging'
set :deploy_to, '/home/ubuntu/vendasnamao'

server '18.205.109.130',
       user: 'ubuntu',
       roles: %w[app db web],
       ssh_options: {
         forward_agent: true,
         auth_methods: %w[publickey],
         keys: %w[~/Projects/conceito/vendas-na-mao/vendasnamao-homolog.pem],
       }
