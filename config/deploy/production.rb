server "34.228.152.152",
  user: "ubuntu",
  roles: %w{app db web},
  ssh_options: {
    forward_agent: true,
    auth_methods: ["publickey"],
    keys: ["~/Projects/conceito/vendas-na-mao/VENMAOAPPLINPROD.pem"]
  }

append :linked_files, "config/secrets.yml", "config/application.yml", "config/database.yml"

project_path = "/var/www/vendas_na_mao/current/"

namespace :deploy do
  after "deploy", "deploy:copy_to"

  task :copy_to do
    on roles(:app) do

      execute "/bin/bash -l -c 'cd #{ project_path } && bundle install && RAILS_ENV=production rails db:migrate db:seed'"

      execute "/bin/bash -l -c 'sudo service nginx-passenger restart'"

    end
  end
end
