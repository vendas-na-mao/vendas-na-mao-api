# Be sure to restart your server when you modify this file.

# Avoid CORS issues when API is called from the frontend app.
# Handle Cross-Origin Resource Sharing (CORS) in order to accept cross-origin AJAX requests.

# Read more: https://github.com/cyu/rack-cors

Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins 'http://localhost:4200',
      'http://187.72.219.1:4200',
      'https://admin.vendasnamao.com.br',
      'https://sandbox.pagseguro.uol.com.br',
      'https://pagseguro.uol.com.br'

    resource '*',
      headers: :any,
      expose: ['Total-Items', 'JWT-Token-Renewed'],
      methods: [:get, :post, :put, :patch, :delete, :options, :head]
  end
end
