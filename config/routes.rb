Rails.application.routes.draw do

  namespace :v1 do
    post 'auth/social_auth'
    post 'auth/normal_auth'
    post 'auth/password_recovery_request'
    match 'auth/password_recovery', to: 'auth#password_recovery', via: [:get, :post]

    resources :clients
    resources :cities, only: [:index, :show]
    resources :states, only: [:index, :show]
    resources :users do
      collection do
        get ':id/orders', to: 'users#orders'
      end
      resources :user_auth_logs
    end
    resources :products
    resources :product_providers do
      resources :products
    end
    resources :bills_to_pays
    resources :receivements
    resources :sells
    resources :sell_groups
    resources :user_preapprovals do
      collection do
        get 'check'
        post 'ios_subscription'
      end
    end
    resources :reports, except: [:index, :show, :create, :update, :destroy] do
      collection do
        get 'client_cost'
        get 'client_cost/:id', to: 'reports#client_cost_by_id'
        get 'sells_and_buys_per_date/:id', to: 'reports#sells_and_buys_per_date'
        # test send email
        get 'send_email', to: 'reports#send_email'
      end
    end

    resources :pagseguro, except: [:index, :show, :create, :update, :destroy] do
      collection do
        get 'preapproval_payments'
        get 'preapprovals'
        get 'get_session'
        post 'subscribe_plan'
        post 'check_plan_subscription'
        post 'notifications'
      end
    end

    get 'address/by_zip_code/:zip_code', to: 'address#by_zip_code'

    resources :notification, except: [:index, :show, :create, :update, :destroy] do
      collection do
        post 'send_notification'
      end
    end

    get  'account_status', to: 'accounts#status'
    post 'subscribe',      to: 'accounts#subscribe'
    post 'unsubscribe',    to: 'accounts#unsubscribe'
  end
end
