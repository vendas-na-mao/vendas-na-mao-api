lock "~> 3.10.0"

set :application, "vendasnamao"
# set :repo_url, "git@bitbucket.org:inacio_farias/vendasnamao-api.git"
# set :repo_url, "git@gitlab.com:vendasnamao/vendas-na-mao-api.git"
set :repo_url, "git@bitbucket.org:vendas-na-mao/vendas-na-mao-api.git"

set :deploy_to, '/var/www/vendas_na_mao'

append :linked_files, "config/database.yml", "config/secrets.yml"

append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "vendor/bundle", "public/system", "public/uploads"
