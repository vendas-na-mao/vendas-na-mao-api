# VENDASNAMAO

A aplicação foi feita usando `ruby 2.4.2` e `rails 5.1.4`.

## Para rodar a aplicação

Caso não o tenha feito, instale o Ruby + Rails e suas dependências.
[ Este guia ](https://gorails.com/setup/ubuntu/16.04) é bem completo e fornece instruções para diversos OSs, deve bastar para concluir tanto a instalação do Ruby quanto do Rails.

+ É recomendado que se use alguma ferramenta para gerenciamento de versões de Ruby e Rails. Eu, particularmente, sugiro a utilização do RVM, e o tutorial acima também instrui como se fazer usando-o.
+ O banco de dados utilizado é o Postgresql.


Após a instalaçao do Ruby & Rails, instale a gem bundler

`gem install bundler`

acesse o diretório onde está a aplicação

`cd path/to/vendasnamao-api`

e rode o comando `bundle install` para instalar todas as dependências do projeto.


Em seguida, renomeie o arquivo `config/database.yml.example` para `config/database.yml` e rode os comandos `rake db:create` e `rake db:migrate` para criar o banco e suas tabelas.

+ Foi posto em `config/database.yml.example` uma configuração em que não se precisa do banco instalado localmente. Caso a prefira, descomente a parte referente a esta configuração em particular e comente a referente ao banco local.


Isso feito, basta rodar o comando `rails s` e o servidor estará acessível no endereço `localhost:3000`
