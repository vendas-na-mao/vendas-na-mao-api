source 'https://rubygems.org'

ruby '2.4.2'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.4'

# Use sqlite3 as the database for Active Record
gem 'sqlite3'

# Use Puma as the app server
gem 'puma', '~> 3.7'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'

# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# postgresql
gem 'pg', '~> 0.21.0'

# JWT auth
gem 'jwt', '~> 1.5.6'

# ActiveRecord scope for api endpoints
gem 'has_scope'

# Simple, Heroku-friendly Rails app configuration using ENV and a single YAML file
gem 'figaro'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors', require: 'rack/cors'

# A Rails plugin to add soft delete.
gem 'acts_as_paranoid', '~> 0.6.0'

group :development, :test do
  # Call 'byebug' anywhere to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
end

group :development do
  # Annotates Rails/ActiveRecord Models, routes, fixtures, and others based on
  # the database schema.
  gem 'annotate', '~> 2.7', '>= 2.7.4'

  # The Listen gem listens to file modifications and notifies you about the changes. Works everywhere!
  gem 'listen', '>= 3.0.5', '< 3.2'

  # Capistrano is a utility and framework for executing commands in parallel on multiple remote machines, via SSH.
  gem 'capistrano', '~> 3.10.0'

  # Rails specific Capistrano tasks.
  gem 'capistrano-rails', '~> 1.3'

  # Passenger support for Capistrano 3.x
  gem 'capistrano-passenger', '~> 0.2.0'

  # RVM integration for Capistrano
  gem 'capistrano-rvm', '~> 0.1.2'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  # Makes spring watch files using the listen gem.
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
