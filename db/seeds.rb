require 'net/http'
require 'json'

module BRPopulate
  def self.states
    puts "Downloading JSON with all Brazil states and cities"
    http = Net::HTTP.new('raw.githubusercontent.com', 443);
    http.use_ssl = true
    JSON.parse http.get('/celsodantas/br_populate/master/states.json').body
  end

  def self.populate
    puts "Populating database..."
    states.each do |state|
      state_obj = State.where(name: state["name"]).first_or_create!(name: state["name"])

      state["cities"].each do |city|
        City.where(name: city["name"], state: state_obj).first_or_create!(name: city["name"], state: state_obj)
      end
    end
    puts "Populating is done!"
  end
end

BRPopulate.populate

puts "Inserting default Product Providers on database..."
[
  'Avon',
  'Hinode',
  'Boticário',
  'Natura',
  'Rommanel',
  'To Be Sunglasses',
  'Mary Kay'
].each do |item|
  ProductProvider.where(name: item).first_or_create!(name: item)
end
puts "Inserting default Products is done!"

puts "Inserting default Admin user on database..."
default_user = {
  name: 'Administrador',
  email: 'S2it.projetos@gmail.com',
  bday: '01/01/1979',
  phone: '(16) 9999-9999',
  user_type: :admin,
  password: 'S2iT@AQA3322',
  password_confirmation: 'S2iT@AQA3322',
  social_auth: false
}
User.where(email: default_user[:email]).first_or_create!(default_user)
puts "Inserting default Admin user is done!"