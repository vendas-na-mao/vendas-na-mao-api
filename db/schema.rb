# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181015142451) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "unaccent"

  create_table "bills_to_pays", force: :cascade do |t|
    t.date "maturity_date"
    t.decimal "price", precision: 8, scale: 2
    t.boolean "paid_out"
    t.bigint "user_id"
    t.bigint "product_provider_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_bills_to_pays_on_deleted_at"
    t.index ["product_provider_id"], name: "index_bills_to_pays_on_product_provider_id"
    t.index ["user_id"], name: "index_bills_to_pays_on_user_id"
  end

  create_table "cities", force: :cascade do |t|
    t.text "name"
    t.bigint "state_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_cities_on_deleted_at"
    t.index ["state_id"], name: "index_cities_on_state_id"
  end

  create_table "clients", force: :cascade do |t|
    t.text "name"
    t.date "bday"
    t.text "email"
    t.string "phone", limit: 200
    t.string "cpf", limit: 200
    t.text "address"
    t.bigint "user_id"
    t.bigint "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["city_id"], name: "index_clients_on_city_id"
    t.index ["deleted_at"], name: "index_clients_on_deleted_at"
    t.index ["user_id"], name: "index_clients_on_user_id"
  end

  create_table "product_providers", force: :cascade do |t|
    t.text "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_product_providers_on_deleted_at"
  end

  create_table "products", force: :cascade do |t|
    t.text "description"
    t.text "code"
    t.decimal "buy_price", precision: 8, scale: 2
    t.decimal "sell_price", precision: 8, scale: 2, default: "0.0"
    t.integer "quantity"
    t.bigint "user_id"
    t.bigint "product_provider_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_products_on_deleted_at"
    t.index ["product_provider_id"], name: "index_products_on_product_provider_id"
    t.index ["user_id"], name: "index_products_on_user_id"
  end

  create_table "receivements", force: :cascade do |t|
    t.date "receivement_date"
    t.decimal "price", precision: 8, scale: 2
    t.bigint "user_id"
    t.bigint "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["client_id"], name: "index_receivements_on_client_id"
    t.index ["deleted_at"], name: "index_receivements_on_deleted_at"
    t.index ["user_id"], name: "index_receivements_on_user_id"
  end

  create_table "sell_groups", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.integer "payment_method", default: 0
    t.integer "parcels"
    t.index ["client_id"], name: "index_sell_groups_on_client_id"
    t.index ["deleted_at"], name: "index_sell_groups_on_deleted_at"
    t.index ["user_id"], name: "index_sell_groups_on_user_id"
  end

  create_table "sells", force: :cascade do |t|
    t.decimal "discount", precision: 8, scale: 2
    t.integer "quantity"
    t.integer "discount_type"
    t.boolean "delivered"
    t.bigint "sell_group_id"
    t.bigint "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_sells_on_deleted_at"
    t.index ["product_id"], name: "index_sells_on_product_id"
    t.index ["sell_group_id"], name: "index_sells_on_sell_group_id"
  end

  create_table "states", force: :cascade do |t|
    t.string "name", limit: 260
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_states_on_deleted_at"
  end

  create_table "user_auth_logs", force: :cascade do |t|
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_auth_logs_on_user_id"
  end

  create_table "user_preapprovals", force: :cascade do |t|
    t.integer "gateway_type"
    t.string "code"
    t.string "tracker"
    t.boolean "active"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_user_preapprovals_on_deleted_at"
    t.index ["user_id"], name: "index_user_preapprovals_on_user_id"
  end

  create_table "user_social_auths", force: :cascade do |t|
    t.string "social_user_id", limit: 260
    t.integer "social_media"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_social_auths_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.text "name"
    t.text "email"
    t.date "bday"
    t.string "phone", limit: 260
    t.integer "user_type"
    t.string "password_digest", limit: 260
    t.boolean "social_auth"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.boolean "premium", default: false, null: false
    t.index ["deleted_at"], name: "index_users_on_deleted_at"
  end

  add_foreign_key "bills_to_pays", "product_providers"
  add_foreign_key "bills_to_pays", "users"
  add_foreign_key "cities", "states"
  add_foreign_key "clients", "cities"
  add_foreign_key "clients", "users"
  add_foreign_key "products", "product_providers"
  add_foreign_key "products", "users"
  add_foreign_key "receivements", "clients"
  add_foreign_key "receivements", "users"
  add_foreign_key "sell_groups", "clients"
  add_foreign_key "sell_groups", "users"
  add_foreign_key "sells", "products"
  add_foreign_key "sells", "sell_groups"
  add_foreign_key "user_auth_logs", "users"
  add_foreign_key "user_preapprovals", "users"
  add_foreign_key "user_social_auths", "users"
end
