class CreateSells < ActiveRecord::Migration[5.1]
  def change
    create_table :sells do |t|
      t.decimal :discount, precision: 8, scale: 2
      t.integer :quantity
      t.integer :discount_type
      t.boolean :delivered
      t.references :sell_group, foreign_key: true
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
