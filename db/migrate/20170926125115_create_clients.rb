class CreateClients < ActiveRecord::Migration[5.1]
  def change
    create_table :clients do |t|
      t.text :name, limit: 1000
      t.date :bday
      t.text :email, limit: 550
      t.string :phone, limit: 200
      t.string :cpf, limit: 200
      t.text :address, limit: 1000
      t.references :user, foreign_key: true
      t.references :city, foreign_key: true

      t.timestamps
    end
  end
end
