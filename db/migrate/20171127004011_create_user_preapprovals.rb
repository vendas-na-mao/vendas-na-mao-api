class CreateUserPreapprovals < ActiveRecord::Migration[5.1]
  def change
    create_table :user_preapprovals do |t|
      t.integer :gateway_type
      t.string :code
      t.string :tracker
      t.boolean :active
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
