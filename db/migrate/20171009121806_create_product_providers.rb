class CreateProductProviders < ActiveRecord::Migration[5.1]
  def change
    create_table :product_providers do |t|
      t.text :name, limit: 550

      t.timestamps
    end
  end
end
