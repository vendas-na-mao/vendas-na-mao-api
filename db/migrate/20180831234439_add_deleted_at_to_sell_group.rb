class AddDeletedAtToSellGroup < ActiveRecord::Migration[5.1]
  def change
    add_column :sell_groups, :deleted_at, :datetime
    add_index :sell_groups, :deleted_at
  end
end
