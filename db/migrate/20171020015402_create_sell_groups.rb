class CreateSellGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :sell_groups do |t|
      t.references :user, foreign_key: true
      t.references :client, foreign_key: true

      t.timestamps
    end
  end
end
