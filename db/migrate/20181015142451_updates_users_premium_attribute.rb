class UpdatesUsersPremiumAttribute < ActiveRecord::Migration[5.1]
  def up
    User.includes(:user_preapproval)
        .where(user_preapprovals: { active: true })
        .find_each do |user|

      user.update_attribute(:premium, true)
    end
  end

  def down
    User.update_all(premium: false)
  end
end
