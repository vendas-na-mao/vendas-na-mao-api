class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.text :name, limit: 1000
      t.text :email, limit: 1000
      t.date :bday
      t.string :phone, limit: 260
      t.integer :user_type
      t.string :password_digest, limit: 260
      t.boolean :social_auth

      t.timestamps
    end
  end
end
