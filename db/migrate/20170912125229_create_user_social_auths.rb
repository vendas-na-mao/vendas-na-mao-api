class CreateUserSocialAuths < ActiveRecord::Migration[5.1]
  def change
    create_table :user_social_auths do |t|
      t.string :social_user_id, limit: 260
      t.integer :social_media
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
