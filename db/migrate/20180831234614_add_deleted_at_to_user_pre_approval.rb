class AddDeletedAtToUserPreApproval < ActiveRecord::Migration[5.1]
  def change
    add_column :user_preapprovals, :deleted_at, :datetime
    add_index :user_preapprovals, :deleted_at
  end
end
