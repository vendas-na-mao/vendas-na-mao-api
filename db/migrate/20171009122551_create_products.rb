class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.text :description, limit: 1000
      t.text :code, limit: 500
      t.decimal :buy_price, precision: 8, scale: 2
      t.decimal :sell_price, precision: 8, scale: 2, default: 0
      t.integer :quantity
      t.references :user, foreign_key: true
      t.references :product_provider, foreign_key: true

      t.timestamps
    end
  end
end
