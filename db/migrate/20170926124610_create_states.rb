class CreateStates < ActiveRecord::Migration[5.1]
  def change
    create_table :states do |t|
      t.string :name, limit: 260

      t.timestamps
    end
  end
end
