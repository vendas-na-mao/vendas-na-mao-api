class AddDeletedAtToProductProvider < ActiveRecord::Migration[5.1]
  def change
    add_column :product_providers, :deleted_at, :datetime
    add_index :product_providers, :deleted_at
  end
end
