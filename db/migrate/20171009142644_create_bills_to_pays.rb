class CreateBillsToPays < ActiveRecord::Migration[5.1]
  def change
    create_table :bills_to_pays do |t|
      t.date :maturity_date
      t.decimal :price, precision: 8, scale: 2
      t.boolean :paid_out
      t.references :user, foreign_key: true
      t.references :product_provider, foreign_key: true

      t.timestamps
    end
  end
end
