class AddDeletedAtToReceivement < ActiveRecord::Migration[5.1]
  def change
    add_column :receivements, :deleted_at, :datetime
    add_index :receivements, :deleted_at
  end
end
