class CreateReceivements < ActiveRecord::Migration[5.1]
  def change
    create_table :receivements do |t|
      t.date :receivement_date
      t.decimal :price, precision: 8, scale: 2
      t.references :user, foreign_key: true
      t.references :client, foreign_key: true
      # t.references :product_provider, foreign_key: true

      t.timestamps
    end
  end
end
