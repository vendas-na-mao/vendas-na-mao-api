class AddAttributesToSellGroup < ActiveRecord::Migration[5.1]
  def change
    add_column :sell_groups, :payment_method, :integer, default: 0
    add_column :sell_groups, :parcels,        :integer
  end
end
