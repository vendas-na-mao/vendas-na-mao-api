class AddDeletedAtToBillsToPay < ActiveRecord::Migration[5.1]
  def change
    add_column :bills_to_pays, :deleted_at, :datetime
    add_index :bills_to_pays, :deleted_at
  end
end
